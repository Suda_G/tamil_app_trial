import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
Generated class for the RestapiService provider.

See https://angular.io/docs/ts/latest/guide/dependency-injection.html
for more info on providers and Angular DI.
*/
@Injectable()
export class RestapiService {
  data: any;
  pageData: any;
  apiUrl = 'http://app.tamilpicturebooks.org:50533';

  constructor(public http: Http) {
    console.log('Hello RestapiService Provider');
  }

  getLevels() {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/levels')
      .map(res => res.json())
      .subscribe(data => {
        this.data = data;
        resolve(this.data);
      });
    });
  }

  getPages(bookId) {
    /**if (this.pageData) {
      return Promise.resolve(this.pageData);
    }**/
    this.pageData = null;
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/pages/' + bookId)
      .map(res => res.json())
      .subscribe(data => {
        this.pageData = data;
        resolve(this.pageData);
      });
    });
  }

}
