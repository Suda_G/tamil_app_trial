import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the BookListService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BookListService {
  data: any;
  apiUrl = 'http://app.tamilpicturebooks.org:50533';

  constructor(public http: Http) {
    console.log('Hello BookListService Provider');
  }

  getBooks(levelId) {
    console.log('get Books API '+ levelId);
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/books/' + levelId)
      .map(res => res.json())
      .subscribe(data => {
        this.data = data;
        resolve(this.data);
      });
    });
  }
}
