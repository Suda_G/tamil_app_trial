import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BookListService } from '../../providers/book-list-service';
import { BookView } from '../book-view/book-view';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the Books page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-books',
  templateUrl: 'books.html',
})
export class Books {
  selecetedLevel;
  levelId;
  books: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public bookListService: BookListService, public loading: LoadingController) {
    console.log('constructer Books');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Books');

    let loader = this.loading.create({
      content: 'Getting latest pages...',
      duration: 5000
    });

    loader.present().then(() => {
      this.selecetedLevel = this.navParams.get('level').level;
      this.levelId = this.navParams.get('level')._id;
      console.log('levelID: ' + this.levelId)
      
      this.getBooks(this.levelId);
      loader.dismiss();
    });
  }

  getBooks(levelId) {
    console.log('getBooks at books.ts');
    this.bookListService.getBooks(levelId)
    .then(data => {
      this.books = data;
    });
  }

  viewBookPage(book) {
    this.navCtrl.push(BookView, {
    book: book
    });
  }

}
