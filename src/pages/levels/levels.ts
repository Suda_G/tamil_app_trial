import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestapiService } from '../../providers/restapi-service';
import { Books } from '../books/books';

/**
* Generated class for the Levels page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-levels',
  templateUrl: 'levels.html',
})
export class Levels {
  levels: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restapiService: RestapiService) {
    this.getLevels();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Levels');
  }

  getLevels() {
    this.restapiService.getLevels()
    .then(data => {
      this.levels = data;
    });
  }

  viewBooks(level) {
    this.navCtrl.push(Books, {
    level: level
    });
  }
}
