import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Levels } from './levels';

@NgModule({
  declarations: [
    Levels,
  ],
  imports: [
    IonicPageModule.forChild(Levels),
  ],
  exports: [
    Levels
  ]
})
export class LevelsModule {}
