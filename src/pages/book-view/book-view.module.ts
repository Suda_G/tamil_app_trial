import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookView } from './book-view';

@NgModule({
  declarations: [
    BookView,
  ],
  imports: [
    IonicPageModule.forChild(BookView),
  ],
  exports: [
    BookView
  ]
})
export class BookViewModule {}
