import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestapiService } from '../../providers/restapi-service';
import { LoadingController } from 'ionic-angular';

/**
* Generated class for the BookView page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-book-view',
  templateUrl: 'book-view.html',
})
export class BookView {
  book;
  bookTitle;
  loaded;
  pages: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restapiService: RestapiService, public loading: LoadingController) {
    //this.getPages();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookView');

    /*let loader = this.loading.create({
    content: 'Getting latest pages...',
    duration: 5000
  });

  loader.present().then(() => {
  this.getPages();
  loader.dismiss();
});*/
let loading = this.loading.create({
  spinner: 'bubbles',
  content: 'We are loading your exciting book..!',
  showBackdrop: true
});

loading.present();
this.getPages();
setTimeout(() => {
  loading.dismiss();
}, 4000);
}

getPages() {
  this.pages = null;
  console.log('getPages BookView');
  this.book = this.navParams.get('book');
  console.log('this.book: '+ this.book);
  console.log('this.book.booktitle: '+ this.book.booktitle);
  this.bookTitle = this.book.booktitle;
  this.restapiService.getPages(this.book._id)
  .then(data => {
    this.pages = data;
  });
}

}
