import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Levels } from '../levels/levels';
import {Main } from '../main/main';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class Home {

  constructor(public navCtrl: NavController) {

  }

  changeRoot() {
    console.log('Changing root to levels');
    this.navCtrl.setRoot(Levels);
  }

}
