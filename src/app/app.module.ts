import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { Home } from '../pages/home/home';
import { Main } from '../pages/main/main';
import { Levels } from '../pages/levels/levels';
import { Books } from '../pages/books/books';
import { BookView } from '../pages/book-view/book-view';
import { RestapiService } from '../providers/restapi-service';
import { BookListService } from '../providers/book-list-service';

@NgModule({
  declarations: [
    MyApp,
    Home,
    Main,
    Levels,
    Books,
    BookView
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Home,
    Main,
    Levels,
    Books,
    BookView
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestapiService,
    BookListService
  ]
})
export class AppModule {}
